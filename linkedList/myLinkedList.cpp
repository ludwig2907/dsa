#include <bits/stdc++.h>

using namespace std;

class node{
    public:
        int data;
        node* next;

        node(int val){
            data=val;
            next=NULL;
        }
};

bool isEmptycheck(node* head){
    return head==NULL;
}

int sizecal(node* head){
    node* temp = head;
    if(isEmptycheck(head)){
        return 0;
    }
    int count=0;
    while(temp!=NULL){
        temp=temp->next;
        count++;
    }
    return count;
}

bool LinearSearch(node* head,int key){
    node* temp=head;
    while(temp!=NULL){
        if(temp->data==key){
            return true;
        }
        temp=temp->next;
    }
    return false;
}

void Add(node* &head,int val){
    node* n=new node(val);

    if(head==NULL){
        head=n;
        return;
    }
    node* temp=head;
    while(temp->next!=NULL){
        temp=temp->next;
    }
    temp->next=n;
}

void Add(node* &head,int val,int index){
    int n=sizecal(head);
    if(index>n||index<0){
        cout<<"Cannot add at that location"<<endl;
        return;
    }
    node* toAdd=new node(val);
    if(index==0){
        toAdd->next=head;
        head=toAdd;
    }
    node* temp=head;
    for(int i=0;i<index-1;i++){
        temp=temp->next;
    }
    node* edit=temp->next;
    temp->next=toAdd;
    toAdd->next=edit;
}

void Delete(node* &head,int index){
    node* temp=head;
    if(index==0){
        head=head->next;
        return;
    }
    for(int i=0;i<index;i++){
        temp=temp->next;
    }
    node* edit = temp->next;
    temp=head;
    for(int i=0;i<index-1;i++){
        temp=temp->next;
    }
    temp->next=edit;
}

void rev(node* &head){
    if(head==NULL){
        cout<<"No values"<<endl;
        return;
    }
    node* current=head;
    node* next;
    node* prev= NULL;
    while(current!=NULL){
        next=current->next;
        current->next=prev;
        prev=current;
        current=next;
    }
    head=prev;
}

node* revK(node* &head,int k){
    node* prevptr=NULL;
    node* currptr=head;
    node* nextptr;

    int count=0;
    while(currptr!=NULL && count<k){
        nextptr=currptr->next;
        currptr->next=prevptr;
        prevptr=currptr;
        currptr=nextptr;
        count++;

    }
    if(nextptr!=NULL){
        head->next=revK(nextptr,k);
    }

    return prevptr;

    
}

void Get(node* head,int index){
    node* temp=head;
    int n=sizecal(head);
    if(index>n-1||index<0){
        cout<<"Out of range"<<endl;
        return;
    }
    for(int i=0;i<index;i++){
        temp=temp->next;
    }
    cout<<temp->data<<endl;
}

void Set(node* head,int val,int index){
    node* temp=head;
    int n = sizecal(head);
    if(index>n-1||index<0){
        cout<<"out of range"<<endl;
        return;
    }

    for(int i=0;i<index;i++){
        temp=temp->next;
    }
    temp->data=val;
}

void printList(node* head){
    node* temp=head;
    while(temp!=NULL){
        cout<<temp->data<<" ";
        temp=temp->next;
    }
    cout<<endl;
}

int main(){
    node* head=NULL;
    Add(head, 1);
    Add(head, 2);
    Add(head,3);
    Add(head,4);
    Add(head,5);
    Add(head,6);
    printList(head);
    node* newhead=revK(head,2);
    printList(newhead);
}
