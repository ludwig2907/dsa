#include<bits/stdc++.h>

using namespace std;

int getMissingNum(int arr[],int n){
    int x=0;
    for(int i=0;i<n;i++){
        x=x^arr[i];
    }

    for(int i =0;i<=n+1;i++){
        x=x^i;
    }

    return x;
}

int main(){
    int arr[]={1,2,3,4,5,7,8,9,10};
    int n=sizeof(arr)/sizeof(arr[0]);

    cout<<getMissingNum(arr, n)<<endl;


}
