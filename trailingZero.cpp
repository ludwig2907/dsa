#include<bits/stdc++.h>

using namespace std;

int trailingZero(int n){
    int count =0;
    for(int i=5;n/i>=1;i=i*5){
        count=count+n/i;
    }

    return count;
}

int main(){
    int n=100;
    cout<<trailingZero(n)<<endl;
}
